-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2020 at 03:09 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laraveltest`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'First', 'This is first blog', '2020-10-04 14:43:51', '2020-10-04 14:43:51', 0),
(2, 'second', 'this is second blog', '2020-10-04 15:44:55', '2020-10-04 15:44:55', 1),
(3, 'third', 'this is third blog', '2020-10-04 15:45:27', '2020-10-04 15:45:27', 1),
(4, 'fourth', 'this is fourth blog', '2020-10-04 15:45:55', '2020-10-04 15:45:55', 1),
(5, 'fifth', 'this is fifth blog', '2020-10-04 15:46:13', '2020-10-04 15:46:13', 1),
(6, 'Sixth', 'this is sixth blog', '2020-10-04 15:46:37', '2020-10-04 15:46:37', 1),
(7, 'seventh', 'this is seventh blog', '2020-10-04 15:46:58', '2020-10-04 15:46:58', 1),
(8, 'Eight', 'this is eight blog', '2020-10-04 15:47:18', '2020-10-04 15:47:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2020_10_03_221905_add_username_field_to_users_table', 3),
(11, '2014_10_12_000000_create_users_table', 4),
(12, '2014_10_12_100000_create_password_resets_table', 4),
(13, '2020_08_26_035024_create_blogs_table', 4),
(14, '2020_10_04_002229_add_phone_field_to_users_table', 5),
(15, '2020_10_04_002556_add_username_field_to_users_table', 6),
(16, '2020_10_04_002744_add_image_field_to_users_table', 7),
(17, '2020_10_04_163033_add_latitude_field_to_users_table', 8),
(18, '2020_10_04_163857_add_longitude_field_to_users_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE `registers` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `longi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`id`, `uid`, `uname`, `email`, `phone`, `img_name`, `lat`, `longi`) VALUES
(1, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '', '19.076090', '72.877426'),
(2, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '1935134989.png', '22.572645', '88.363892'),
(3, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '340200908.png', '13.067439', '80.237617'),
(4, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '1432146148.png', '28.704060', '77.102493'),
(5, '7', NULL, NULL, '123456', NULL, NULL, NULL),
(6, '8', 'raaj', NULL, '123456', NULL, '28.7041', '77.1025'),
(7, '10', 'xontech', NULL, '123456', '1878969024.png', '28.7041', '28.7041'),
(8, '11', 'final', NULL, '12345678', '1861384405.png', '28.7041', '77.1025');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` bigint(20) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `username`, `image`, `latitude`, `longitude`) VALUES
(1, 'Neeraj Rai', 'developer1.raaj@gmail.com', NULL, '$2y$10$9vg9ymLYOciCEeDaHMAsu.Iv5HjXSNJ1MH2T515ZkZxOGDIJvPobm', NULL, '2020-10-03 18:59:51', '2020-10-03 18:59:51', 0, '', '', '', ''),
(2, 'Neeraj Rai', 'devekjloper1.raaj@gmail.com', NULL, 'hjgvbfdxc', NULL, '2020-10-03 19:18:53', '2020-10-03 19:18:53', 9810407085, 'admin', '1908873800.jpg', '', ''),
(3, 'Nraj', 'raj@gmail.com', NULL, '$2y$10$bSrdUkBGDFq5vwsEr0Bq6.6eDEo4evRNSdQrgt7w13TT6j5oszV8m', NULL, '2020-10-03 19:22:46', '2020-10-03 19:22:46', 1234567898, 'admin', '992352819.jpg', '', ''),
(4, 'newtest', 'newtest@gmail.com', NULL, '$2y$10$tAlcRGdgRtye.9FLL6ZZ0.VOl2zs9I9W5NDvb4Rt3r7Q885f65GsO', NULL, '2020-10-03 19:26:53', '2020-10-03 19:26:53', 9810407085, 'admin', '142377986.JPG', '', ''),
(5, 'Neeraj Rai', 'neerajtest@gmail.com', NULL, '$2y$10$ZJA7EDNHaVR/Rx97GHCj/.hVWbWZ28tSDVUHUh/CYV3SqkPYP2FSq', NULL, '2020-10-04 10:34:56', '2020-10-04 10:34:56', 9810407085, 'Nk', '2119216707.jpg', '', ''),
(6, 'Neeraj Rai', 'neerajtest123@gmail.com', NULL, '$2y$10$bwTZujvhKlzvuNM06kxFcuhncIL2hMrufAnluKALUeeyrCMq.iuEq', NULL, '2020-10-04 11:51:48', '2020-10-04 11:51:48', 9810407085, 'admin', '274739105.JPG', '28.7041', '77.1025');

-- --------------------------------------------------------

--
-- Table structure for table `usersq`
--

CREATE TABLE `usersq` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `longi` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usersq`
--

INSERT INTO `usersq` (`id`, `name`, `password`, `email`, `phone`, `img_name`, `lat`, `longi`, `updated_at`, `created_at`) VALUES
(1, 'test', '123', 'test@gmail.com', '', '', '', '', '2020-09-10 04:23:55', NULL),
(2, 'test2', '123', 'test2@gmail.com', '', '', '', '', '2020-09-10 04:23:55', NULL),
(3, 'Neeraj Rai', '$2y$10$Ch3hHwRJw5H4pwIY2W89dOQFmnE40OZ2MpIk/XdXRUcP7/GY7KDs6', 'developer1.raaj@gmail.com', '', '', '', '', '2020-09-09 22:56:33', '2020-09-09 22:56:33'),
(4, 'Neeraj Rai', '$2y$10$EnkflWTP0ZhNVs1cQmllW.j2Vx5eh4xOYlJCtc7QggvzZBkDeAFMG', 'marketing@valuecoders.com', NULL, NULL, NULL, NULL, '2020-09-10 03:15:59', '2020-09-10 03:15:59'),
(5, 'Neeraj Rai', '$2y$10$h30n.k8TbkQoBa0r5B.V7O6HAQt.rpGFSRyYdh6ToOJlUCmMcUs5K', 'developer15.raaj@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 03:18:35', '2020-09-10 03:18:35'),
(6, 'Neeraj Rai', '$2y$10$fHVDZSx/TJzkqJRq7vWLI.p/7GoKPHQOQo./M57kJUZbSVI4YkKma', 'neerxaj6@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:01:46', '2020-09-10 04:01:46'),
(7, 'Neeraj Rai', '$2y$10$Dbih6MB/vfENM.MXNkiXgeELk2vuN93wSLRMbMhWddRebwMr6/pLS', 'neeraj.rajji86@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:20:14', '2020-09-10 04:20:14'),
(8, 'Neeraj Rai', '$2y$10$j95/aOLHBcWxc1IC6DpWsOUAY9tP94PXWtg9lj/1dwfZ5eF9RoFWq', 'developjjer1.raaj@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:43:52', '2020-09-10 04:43:52'),
(9, 'Neeraj Rai', '$2y$10$9KNYQ/IlAvKJ.jBI4zt0b.Fg2ZRUvGyK6AOhODAlLV6A8sVmfSd7m', 'neeraj5@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:09:13', '2020-09-10 05:09:13'),
(10, 'Neeraj Rai', '$2y$10$MMfP5lB/82SAz3eeUyHhVOV2z/ZuMzoQmVxioiRaK2tx8TkqizhVi', 'xonestkkl@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:12:59', '2020-09-10 05:12:59'),
(11, 'ffdd', '$2y$10$MKshk/NkBUMJkDxLQK4jK.KQlgZ2IJ7XPWR3.Jm6m0M8LGGqQhkP2', 'neffdfderaj.rai86@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:17:44', '2020-09-10 05:17:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `usersq`
--
ALTER TABLE `usersq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `registers`
--
ALTER TABLE `registers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `usersq`
--
ALTER TABLE `usersq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
