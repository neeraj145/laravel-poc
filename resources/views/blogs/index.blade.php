@extends('blogs.layout')
 
@section('content')
    <div class="row">
	
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Blogs</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('blogs.create') }}"> Create new blogs</a>
				
				<form>
						<select id="pagination">
							<option value="5" @if($items == 5) selected @endif >5</option>
							<option value="10" @if($items == 10) selected @endif >10</option>
							<option value="25" @if($items == 25) selected @endif >25</option>
						</select>
					</form>           
					</div>
			 
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>@sortablelink('id')</th>
            <th>@sortablelink('title')</th>
            <th>Description</th>
			<th>@sortablelink('created_at')</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($blogs as $blog)
        <tr>
            <td>{{ $blog->id }}</td>
            <td>{{ $blog->title }}</td>
            <td>{{ $blog->description }}</td>
			<td>{{ $blog->created_at->format('d-m-Y') }}</td>
            <td>
                <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('blogs.show',$blog->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
					
				<?php if($blog->status==0) {?>	
				<a class="btn btn-primary" id="user-{{$blog->id}}"  onclick="changeUserStatus({{ $blog->status }}, {{ $blog->id }});">Draft</a>
				<?php } else {?>
				<a class="btn btn-primary" id="user-{{$blog->id}}"  onclick="changeUserStatus({{ $blog->status }}, {{ $blog->id }});">Active</a>
				<?php } ?>
					 
					
   
                   @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $blogs->links() !!}
      
@endsection

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
$(document).on('change', '#pagination', function() {
  window.location = "{!! $blogs->url(1) !!}&items=" + this.value; 
});


/*Start: Ajax Code for change status */
function changeUserStatus(status, id) {
	
	
    let _token = $('meta[name="csrf-token"]').attr('content');
	
	if(status===1){
		status=0;
	}  
	else if(status===0){
		status=1;
	}
    $.ajax({
        url: 'chnage',
        type: 'post',
		header:{
          'X-CSRF-TOKEN': _token
        },
        data: {
            _token: _token,
            id: id,
            status: status,
			dataType: 'json', 
			contentType:'application/json',	
        },
        success: function (result) {
			
			if(result.success!=''){
				if(status==1){
				$('#user-'+id).html('Active');
				$('#user-'+id).attr('onclick','changeUserStatus(1, '+id+')');
				}
				if(status==0){
				$('#user-'+id).html('Draft');
				$('#user-'+id).attr('onclick','changeUserStatus(0, '+id+')');
				}
			}
			
        }
    });
}
/*End: Ajax Code for change status */

	
   



</script>