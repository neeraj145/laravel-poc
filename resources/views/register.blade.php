
	@extends('layout')
    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
									
									@if (count($errors) > 0)
										<div class="alert alert-danger">
											<strong>Whoops!</strong> There were some problems with your input.
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
									
									
									
                                        <form action="{{url('post-register')}}" method="POST" id="regForm" enctype="multipart/form-data">
										<div id="stp1">
										{{ csrf_field() }}
                                            <div class="form-group">
												<label class="small mb-1" for="inputFirstName">Name</label>
												<input class="form-control py-4" id="inputFirstName" type="text" name="name" placeholder="Enter Full Name" />
												<?php /*@if ($errors->has('name'))
												  <span class="error">{{ $errors->first('name') }}</span>
												  @endif */?>  
											</div>
											<br/>
											
                                            <div class="form-group">
												<label class="small mb-1" for="inputEmailAddress">Email</label>
												<input class="form-control py-4" id="inputEmailAddress" type="email" aria-describedby="emailHelp" name="email" placeholder="Enter email address" />
												<?php /*@if ($errors->has('email'))
												  <span class="error">{{ $errors->first('email') }}</span>
												@endif */?>
											</div>
											<br/>
											<div class="form-group">
												<label class="small mb-1" for="inputPhone">Phone</label>
												<input class="form-control py-4" id="inputPhone" type="text" name="phone" placeholder="Phone">
												<?php /*@if ($errors->has('phone'))
												  <span class="error">{{ $errors->first('phone') }}</span>
												@endif */ ?>
											</div>
											
											<div class="card-footer text-center">
												<div class="small"><a id="stpb" href="javascript:void(0);">Step 2</a></div>
											</div>
											
											</div>
											
											<br/>
											<div id="stp2" style="display:none">
											
											<div class="form-group">
												<label class="small mb-1" for="inputUname">Username</label>
												<input class="form-control py-4" id="inputUname" type="text" name="username" placeholder="User Name">
											</div>
											
											<br/>
											<div class="form-group">
												<label class="small mb-1" for="inputImage">Image</label>
												<input type="file" name="image">
											</div>
											<br/>
											<div class="form-group">
												<label class="small mb-1" for="inputImage">Google coordinate</label>
												<input placeholder="Latitude" type="text" name="latitude">
												<input placeholder="Longitude" type="text" name="longitude">
												
											</div>
											
											<br/>
											<div class="form-group">
												<label class="small mb-1" for="inputPassword">Password</label>
												<input class="form-control py-4" id="inputPassword" type="password" name="password" placeholder="Enter password" />
												<?php /*@if ($errors->has('password'))
												  <span class="error">{{ $errors->first('password') }}</span>
												@endif */?>
											</div>
											<div class="card-footer text-center">
												<div class="small"><a id="stpb1" href="javascript:void(0);">Step 1</a></div>
											</div>
											<br/>
                                            <div class="form-group mt-4 mb-0">
								                <button class="btn btn-primary btn-block" type="submit">Create Account</button>
											</div>
											
											</div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="{{url('login')}}">Have an account? Go to login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{url('assets/js/scripts.js')}}"></script>
		
		<script>
		$( document ).ready(function() {
		$('#stpb').click(function(){
			
			$('#stp2').show();
			$('#stp1').hide();
			
		});
		
		$('#stpb1').click(function(){
			
			$('#stp1').show();
			$('#stp2').hide();
			
		});
		
		
		
		});

		</script>
	