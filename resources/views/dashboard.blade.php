<?php //echo '<pre>'; print_r($records); die;?>


	@extends('layout')
    
        
        <div id="layoutSidenav">
            
            <div id="layoutSidenav_content">
                
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard</h1>
                        <nav class="sb-topnav navbar navbar-expand navbar-dark">
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    
                    <div class="" aria-labelledby="userDropdown">
                        <div class="dropdown-divider"></div>
                        <a style="float:right" class="dropdown-item" href="{{url('logout')}}">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
                        <div class="row">
                            <div class="col-xl-8 col-md-6">
                                <div class="card bg-gradient-info text-black mb-4">
								
                                    <div class="card-body">
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Name:</div>
									  <div class="col-md-9">
										{{ $records->username  }}
									  </div>
									</div>
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Phone:</div>
									  <div class="col-md-9">
										{{ $records->phone  }}
									  </div>
									</div>
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Email:</div>
									  <div class="col-md-9">
										{{ $records->email  }}
									  </div>
									</div>
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Image:</div>
									  <div class="col-md-9">
										<img height="100" width="100" src="images/{{ $records->image }}" >
									  </div>
									</div>
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Latitude:</div>
									  <div class="col-md-9">
										{{ $records->latitude  }}
									  </div>
									</div>
									
									<div class="row mb-2">
									  <div class="col-md-3 text-muted">Longitude:</div>
									  <div class="col-md-9">
										{{ $records->longitude  }}
									  </div>
									</div>
									
									</div>
								</div>	
							</div>	
						</div>	
						
						<section id="sidebar">
							<div id="directions_panel"></div>
						</section>

						<section id="main">
						<div id="map_canvas" style="width: 70%; height: 500px;"></div>
						</section>
						
						
					</div>	
				</div>
			</div>	
<style type="text/css">
body { font: normal 14px Verdana; }
h1 { font-size: 24px; }
h2 { font-size: 18px; }
#sidebar { float: right; width: 30%; }
#main { padding-right: 15px; }
.infoWindow { width: 220px; }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
//<![CDATA[

var map;

// Ban Jelačić Square - Center of Zagreb, Croatia
var center = new google.maps.LatLng(20.5937, 78.9629);

function init() {

var mapOptions = {
zoom: 5,
center: center,
mapTypeId: google.maps.MapTypeId.ROADMAP
}

//map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

/* var marker = new google.maps.Marker({
map: map,
position: center,
}); */



var passedArray = <?php echo json_encode($records); ?>




var map = map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var infowindow = new google.maps.InfoWindow(), marker, lat, lng;
        var json= passedArray;

        for( var o in json ){
			
			console.log(json.name);

            lat = json.latitude;
            lng=json.longitude;
          //  name= json[ o ].name;
            //email= json[ o ].email;
			
			var boxText = "<div class='mapLocationBox'>";
                boxText += "<h2>" + json.name + "</h2>" + "<p>" + json.email + "<br>" + json.phone +  "</p>";
                boxText += "</div>";

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat,lng),
                name:name,
                map: map
            }); 
            google.maps.event.addListener( marker, 'click', function(e){
                //infowindow.setContent( this.name);
				infowindow.setContent(boxText);
                
                infowindow.open( map, this );
            }.bind( marker ) );
        }

 


} 
//]]>
</script>
</head>
<body onload="init();">




        
    </body>
</html>