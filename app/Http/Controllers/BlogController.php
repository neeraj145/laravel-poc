<?php
  
namespace App\Http\Controllers;
  
use App\Blog;
use Illuminate\Http\Request;

use DB;
use Auth;
use Redirect;
  
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
	   if(Auth::check()){
	   
	   $items = $request->items ?? 5;
	   $blogs = Blog::latest()->paginate($items);
		
		
        return view('blogs.index',compact('blogs'))
            ->with('i', (request()->input('page', 1) - 1) * 5)->withItems($items);
	   }
	   
	   return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
		return view('blogs.create');
		}
		return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
		$request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
  
        Blog::create($request->all());
   
        return redirect()->route('blogs.index')
                        ->with('success','Blog created successfully.');
		}
				return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
		if(Auth::check()){
        return view('blogs.show',compact('blog'));
		}
		return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        if(Auth::check()){
		return view('blogs.edit',compact('blog'));
		}
		return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        
		if(Auth::check()){
		$request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
  
        $blog->update($request->all());
  
        return redirect()->route('blogs.index')
                        ->with('success','Blog updated successfully');
						
		}

			return Redirect::to("login")->withSuccess('Opps! You do not have access');

			
    }
	
	public function changeStatus(Request $request, Blog $blog)
    {
		
	  
	  
	  //$blog->update(['status' => $request->status]);
	  $affected = DB::table('blogs')
              ->where('id', $request->id)
              ->update(['status'=>$request->status]);
	  
	//  $blog->where('id','=',$request->status)->update(['status' => $request->status]);
	  
	   

        return response()->json(['success'=>'Status changed successfully.']);

       //return response()->json(['success'=>'Status changed successfully.']);
	  /*  return redirect()->route('blogs.index')
                        ->with('success','Blog updated successfully'); */
    }
  
  
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();
  
        return redirect()->route('blogs.index')
                        ->with('success','Blogs deleted successfully');
    }
}