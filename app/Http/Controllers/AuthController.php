<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Validator,Redirect,Response;
Use App\User;
Use App\Account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }  
 
    public function register()
    {
        return view('register');
    }
     
    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
    }
 
    public function postRegister(Request $request)
    {  
        request()->validate([
        'name' => 'required|regex:/^[a-zA-Z\s]*$/',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
		'phone' => 'required|digits:10',
		'username' => 'required|alpha',
		'latitude' => 'required',
		'longitude' => 'required',
		'image' => 'required|image|mimes:jpeg,png,jpg,gif|min:5120'
		],[
        'password.regex' => 'Password must have atleast one capital letter, one number and one special charactor',
		'image.min' => 'Image must be more than 5 MB',
    ]); 
         
        $data = $request->all();
		
		//echo '<pre>';
		//print_r($data); die;
		
		$image = $request->file('image');
				$new_name = rand().'.'.$image->getClientOriginalExtension();
			$res = $image->move(public_path("images"),$new_name);
		DB::enableQueryLog();
		if($res){
		//$check = $this->create($data);
		User::create([
        'name'  =>  $data['name'],
        'email'  =>  $data['email'],
        'phone' =>  $data['phone'],
        'username'  =>  $data['username'],
        'password'   =>  bcrypt($data['password']),
        'image'     =>  $new_name,
		'latitude'  =>  $data['latitude'],
		'longitude'  =>  $data['longitude'],
    ]);
		
		}
		// Your Eloquent query executed by using get()
		//DB::enableQueryLog(); // Enable query log
		//dd(DB::getQueryLog()); // Show results of log
		return Redirect::to("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
     
    public function dashboard()
    {
 
      if(Auth::check()){
		  
		  $data = Session::all();
		  
		  $id = auth()->user()->id;
		  $records = User::find($id);	
		 //$records = DB::table('users')->where('uid','=',$uid)->get();
		
	//echo '<pre>';
		//print_r($records); die;
		
     //  return view ('show')->with('records',$records);
        return view('dashboard')->with('records',$records);;
		
		
		
		
      }
       return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
 
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
     
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
}
