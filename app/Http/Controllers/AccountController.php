<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use DB;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
	
	public function save(Request $req)
    {
		//print_r($req->input()); die;
        //
		$user = new Account;
		
		
		if ($req->hasFile('image')) {
            //  Let's do everything here
            if ($req->file('image')->isValid()) {
                //
                /* $validated = $req->validate([
                    'name' => 'string|max:40',
                   'image' => 'mimes:jpeg,png|max:5120',
                ]); */
				
				$req->validate([
						'name' => 'required',
						'email' => 'required|email',
						'phone' => 'required|min:10|numeric',
						'password' => 'required|string|min:6|max:10|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
					]);
				
				$image = $req->file('image');
				$new_name = rand().'.'.$image->getClientOriginalExtension();
				$image->move(public_path("images"),$new_name);
				
				$user->name = $req->name;
				$user->email = $req->email;
				$user->phone = $req->phone;
				$user->img_name = $new_name;
				$user->lat = $req->lat;
				$user->longi = $req->long;
				$user->save();
				
				
					return back()->with('success','uploaded successfully');
                
            }
        }
      
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function showData()
    {
      //  return view('show');
	  
	  
		
		$records = DB::table('registers')->get();
		
		//echo '<pre>';
		//print_r($records);
		
       return view ('show')->with('records',$records);
	  //return view('show')->with('name', 'Victoria');
    }
	 
	 
    public function create()
    {
        //
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
