<?php
  
namespace App;
  
use Illuminate\Database\Eloquent\Model;
   
class Blog extends Model
{
    protected $fillable = [
        'title', 'description', 'status'
      ];
	 public $sortable = ['id', 'title', 'created_at', 'updated_at']; 
	  
	  
}